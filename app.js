// # Polycam Devops Technical Interview
//
// Important: these steps are under-specified. Take a look at error messages
// and try and figure things out before looking at source code.
//
//
// ## Setup (for running locally outside of Docker):
//
// * copy the `package.json` contents.
// * `npm install`.
// * `mkdir local-bucket-dir`.
// * `node app.js`
//
//
// ## Commands to test the app:
//
// * `curl localhost:3000`
// * `curl localhost:3000/object?key=/my-key`
// * `curl -X PUT localhost:3000/object?key=/my-key`
//
//
// ## Interview steps
// 1. Discovery and setup.
// 2. Dockerize this application, using a local volume.
// 3. Switch to using a remote bucket. (and GOOGLE_APPLICATION_CREDENTIALS pointing to gcp_creds.json)
// 4. Terraform or other IAC for setting up infra.
//
//
// package.json:
//
// {
//   "name": "docker-tf",
//   "version": "1.0.0",
//   "description": "",
//   "type": "module",
//   "main": "app.js",
//   "scripts": {
//     "test": "echo \"Error: no test specified\" && exit 1"
//   },
//   "author": "",
//   "license": "ISC",
//   "dependencies": {
//     "@google-cloud/storage": "^7.7.0",
//     "express": "^4.18.3"
//   }
// }
//
// gcp_creds.json:
//
// See follow-up email
//
import fs from 'fs/promises';
import path from 'path';
import express from 'express';
import { Storage } from '@google-cloud/storage';

const app = express();
const port = 3000;

const LOCAL_BUCKET_DIR = process.env.LOCAL_BUCKET_DIR;
const GOOGLE_APPLICATION_CREDENTIALS = process.env.GOOGLE_APPLICATION_CREDENTIALS;
const REMOTE_BUCKET = process.env.REMOTE_BUCKET;

if (LOCAL_BUCKET_DIR === undefined && GOOGLE_APPLICATION_CREDENTIALS === undefined) {
  throw new Error('Must define either LOCAL_BUCKET_DIR (step 1-2) or GOOGLE_APPLICATION_CREDENTIALS and REMOTE_BUCKET (step 3)');
}

const mode = GOOGLE_APPLICATION_CREDENTIALS ? 'google' : 'local';

if (mode === 'google' && !REMOTE_BUCKET) {
  throw new Error('Must define REMOTE_BUCKET=polycam-interview-devops-001-data');
}

function lazy(func) {
  let cache;

  return () => {
    if (cache !== undefined) {
      return cache.value;
    } else {
      cache = {
        value: func(),
      };
      return cache.value;
    }
  };
}

const lazyStorage = lazy(() => {
  return new Storage({ keyFilename: GOOGLE_APPLICATION_CREDENTIALS });
});

app.use(express.raw());

app.get('/', async (req, res) => {
  const key = req.query.key ?? '/';
  if (!key.startsWith('/')) {
    res.status(422).send('Invalid key: must start with /');
    return;
  }

  switch (mode) {
    case 'local': {
      try {
        const results = await fs.readdir(path.join(LOCAL_BUCKET_DIR, key));
        res.send(JSON.stringify(results));
      } catch (err) {
        if (err.code === 'ENOENT') {
          res.status(404).send(`Key not found: ${key}`);
        } else {
          console.error(err);
          res.status(500).send('Internal server error');
        }
      }
      break;
    }
    case 'google': {
      try {
        const storage = lazyStorage();
        const bucket = storage.bucket(REMOTE_BUCKET);
        const result = await bucket.getFiles({
          autoPaginate: false,
          delimiter: '/',
          prefix: key,
        });
        const files = result[0].map((f) => f.name);
        const prefixes = result[2].prefixes ?? [];
        res.send(JSON.stringify(files.concat(prefixes)));
      } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
      }
      break;
    }
  }
});

app.get('/object', async (req, res) => {
  const key = req.query.key ?? '/';
  if (!key.startsWith('/')) {
    res.status(422).send('Invalid key: must start with /');
    return;
  }

  switch (mode) {
    case 'local': {
      try {
        const result = await fs.readFile(path.join(LOCAL_BUCKET_DIR, key));
        res.send(result);
      } catch (err) {
        if (err.code === 'ENOENT') {
          res.status(404).send(`Key not found: ${key}`);
        } else {
          console.error(err);
          res.status(500).send('Internal server error');
        }
      }
      break;
    }
    case 'google': {
      try {
        const storage = lazyStorage();
        const bucket = storage.bucket(REMOTE_BUCKET);
        const file = bucket.file(key);
        const result = await file.download();
        const contents = result[0];
        res.send(contents);
      } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
      }
      break;
    }
  }
});

app.put('/object', async (req, res) => {
  const key = req.query.key ?? '/';
  if (!key.startsWith('/')) {
    res.status(422).send('Invalid key: must start with /');
    return;
  }

  if (!req.body) {
    res.status(422).send('Invalid body: empty');
    return;
  }

  if (!(req.body instanceof Buffer)) {
    res.status(422).send('Invalid body: must be an application/octet-stream');
    return;
  }

  switch (mode) {
    case 'local': {
      try {
        await fs.writeFile(path.join(LOCAL_BUCKET_DIR, key), req.body);
        res.send(`${req.body.length} bytes written to ${key}`);
      } catch (err) {
        if (err.code === 'ENOENT') {
          res.status(404).send(`Key not found: ${key}`);
        } else {
          console.error(err);
          res.status(500).send('Internal server error');
        }
      }
      break;
    }
    case 'google': {
      try {
        const storage = lazyStorage();
        const bucket = storage.bucket(REMOTE_BUCKET);
        const file = bucket.file(key);
        await file.save(req.body);
        res.send(`${req.body.length} bytes written to ${key}`);
      } catch (err) {
        console.error(err);
        res.status(500).send('Internal server error');
      }
      break;
    }
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});