#example docker file

# Filename: Dockerfile
FROM node:20-alpine
WORKDIR /usr/src/app
COPY package.json ./
COPY app.js ./
RUN npm install
RUN mkdir /localvolume/
COPY . .
EXPOSE 3000
env LOCAL_BUCKET_DIR=/localvolume/
CMD ["node", "app.js"]

