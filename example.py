   
# Iterating over dictionary keys
person = {"name": "John", "age": 30, "city": "New York"}
for key in person.keys():
    print(key)

   
# Iterating over dictionary values
person = {"name": "John", "age": 30, "city": "New York"}
for value in person.values():
    print(value)